Tutorial
--------

Installing
~~~~~~~~~~

To install from pypi simply use:

.. code-block:: bash

    pip install ctp-builder

Creating configuration
~~~~~~~~~~~~~~~~~~~~~~~

To create a new CTP configuration we first determine the template we want to
use. The template is a pre-defined configuration that we can use as a starting point.
In the current version of CTP-builder there are three templates available: ``full_service``,
``passthrough_service`` and ``passthrough_service_healthcheck``. The ``full_service``
template is a more complex configuration that includes multiple stages and services.
The ``passthrough_service`` template is a  configuration that can be used to pass through
data from one location to another or to organize data in a directory structure. The
``passthrough_service_healthcheck`` templates is similar to the ``passthrough_service``
but is more suited for use in k8s deployments.

In this tutorial we will create a simple passthrough pipeline that will receive data using 
a DICOM importer port and will export data using HTTP.
First we start with stating the version of the ctpbuilder configuration in this case 
version 1 and we start by selecting the template we want to use. In this case we are using
the passthrough_service template.

.. code-block:: yaml

    version: 1
    template: passthrough_service

After selecting the template we can start defining the server configuration. In this case we
define the port that the server will listen on and the storage location where the
temporary/quarantine data will be stored.

.. code-block:: yaml
    
    version: 1
    template: passthrough_service
    server:
        port: 8080
        storage: /mnt/data

Next we define the pipeline configuration. In this case we define the name of the pipeline. 
For a passthrough pipeline we need to define the import and export type and depending on the 
the type we eihter define port, url or path. In this case we are using the DICOM import type
and Http export type. That means that we need to define the port the DICOM-importer will 
listen to and the url where the data will be exported to.

.. code-block:: yaml
    
    version: 1
    template: passthrough_service
    server:
        port: 8080
        storage: /mnt/data
    pipeline:
        name: anonymization_project
        import_type: Dicom
        import_prt: 11112
        export_type: Http
        export_url: https://ctp.example.com/data:443

The configuration file is now complete and we can save it to a file ``config.yaml``. Using the command line 
interface we can convert this configuration file to a full CTP configuration.

.. code-block:: bash

    ctpbuild generate --config config.yaml -o /path/to/output

The output folder thould now contain a full CTP configuration (config.xml) that can be used to start the CTP service.
