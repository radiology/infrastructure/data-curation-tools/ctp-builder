.. ctp-builder documentation master file, created by
   sphinx-quickstart on Thu Feb  6 14:44:14 2025.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

ctp-builder documentation
=========================

CTP-builder is tool for building and complete CTP (Clinical Trial Processor) 
configuration based on a simple configuration files. The idea is have templates
for specific needs for CTP and easily create new configurations based on these
templates. This also makes it easier to deploy CTP in a k8s cluster environment.
CTP configuration can be created in a simple YAML file and then converted to
a full CTP configuration using either the command line interface or python API.

For example, the following YAML configuration file:

.. code-block:: YAML

   version: 1
   template: full_service
   server:
      port: 8080
      storage: /mnt/data
   pipeline:
      name: anonymization_project
      import_type: Archive
      import_path: /data/archive
      export_type: Directory
      export_path: /data/organized

is turned into a full CTP configuration including script files and looup tables.

.. code-block:: XML

   <Configuration>
      <Server
         maxThreads="20"
         port="8080"/>
      <Pipeline name="anonymization_project">
         <ArchiveImportService
            class="org.rsna.ctp.stdstages.ArchiveImportService"
            logConnections="rejected"
            name="ArchiveImport"
            treeRoot="/data/archive"
            quarantine="/mnt/data/quarantine/anonymization_project/ArchiveImport"
            root="/mnt/data/root/anonymization_project/ArchiveImport"
            acceptDicomObjects="yes"/>
         <ObjectTracker
            name="ObjectTracker"
            class="org.rsna.ctp.stdstages.ObjectTracker"
            root="/mnt/data/root/anonymization_project/ObjectTracker" />
         <DicomFilter
            class="org.rsna.ctp.stdstages.DicomFilter"
            name="DicomFilter"
            quarantine="/mnt/data/quarantine/anonymization_project/DicomFilter"
            root="/mnt/data/root/anonymization_project/DicomFilter"
            script="scripts/ModalityFilter.script"/>
         <DicomFilter
            class="org.rsna.ctp.stdstages.DicomFilter"
            name="DicomFilter"
            quarantine="/mnt/data/quarantine/anonymization_project/DicomFilter"
            root="/mnt/data/root/anonymization_project/DicomFilter"
            script="scripts/SeriesDescriptionBlacklist.script"/>
         <DicomAnonymizer
            id = "Anonymizer"
            class="org.rsna.ctp.stdstages.DicomAnonymizer"
            lookupTable="resources/lookuptable"
            name="DicomAnonymizer"
            quarantine="/mnt/data/quarantine/anonymization_project/DicomAnonymizer"
            root="/mnt/data/root/anonymization_project/DicomAnonymizer"
            script="scripts/DicomAnonymizer.script"/>
         <DirectoryStorageService
            class="org.rsna.ctp.stdstages.DirectoryStorageService"
            name="DirectoryExport"
            quarantine="/mnt/data/quarantine/anonymization_project/DirectoryExport"
            root="/mnt/data/root/anonymization_project"
            structure="(0010,0020)/(0008,0020)/0020,000d/(0008,103e)"
            path="/data/organized"/>
      </Pipeline>
   </Configuration> 

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorial.rst
   default_templates.rst
   custom_templates.rst
   reference.rst
   changelog.rst
