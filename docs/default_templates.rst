Default templates
-----------------

In the current package there's a few of templates included which are
described on this page. These can be used as examples and the results should
be carefully checked when using in actual projects.

Passthrough
~~~~~~~~~~~

This is a simple template that passes files through the pipeline without any modifaction or filtering. There's only two things that need to be defined:
How to import the data and where to export the data.
Depending on the type of import either `DICOM`, `HTTP`, `Directory`, or `Archive` the configuration will be different, we need to define either a import_port or import_path:

.. list-table:: Title
   :widths: 25 25 50
   :header-rows: 1

   * - Option
     - Values
     - Description
   * - server.storage
     - string
     -
   * - server.port
     - integer
     -
   * - pipeline.import_type
     - ['DICOM', 'Http']
     - Requires: import_port 
   * - pipeline.import_type
     - ['Directory', 'Archive']
     - Requires: import_path
   * - pipeline.import_port
     - integer
     - 
   * - pipeline.import_path
     - string
     -
    
   * - pipeline.export_type
     - ['Dicom', 'Http']
     - Requires: export_url
   * - pipeline.export_type
     - ['Directory', 'File']
     - Requires: export_path
   * - pipeline.export_url
     - string
     -
   * - pipeline.export_path
     - string
     - 

passthrough with healthcheck
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Is the same as the passthrough template but with a filter that rejects all
DICOM files that contain "CTPHealthCheck" as patient comments. This can be
helpfull when deploying CTP in a k8s cluster environment and you want to 
perform health checks either on the a DICOM or Http import service. 

.. list-table:: Title
   :widths: 25 25 50
   :header-rows: 1

   * - Option
     - Values
     - Description
   * - server.storage
     - string
     -
   * - server.port
     - integer
     -
   * - pipeline.import_type
     - ['DICOM', 'Http']
     - Requires: import_port 
   * - pipeline.import_type
     - ['Directory', 'Archive']
     - Requires: import_path
   * - pipeline.import_port
     - integer
     - 
   * - pipeline.import_path
     - string
     -
    
   * - pipeline.export_type
     - ['Dicom', 'Http']
     - Requires: export_url
   * - pipeline.export_type
     - ['Directory', 'File']
     - Requires: export_path
   * - pipeline.export_url
     - string
     -
   * - pipeline.export_path
     - string
     - 

full_service
~~~~~~~~~~~~

The template is designed to be used as a starting point for a new project. 
It is a more complex template that includes a mutlitude of filters and 
anonymizers. The filter are used to reject all modalilties that are not 
supposed to be part of the project furthermore there is a default template
to discard all filesw ith certain series descriptions that we know might
contain personal health information (PHI). Furthermore there are DICOM
anonymizers to remove PHI from the DICOM files. 
Also there's the option to set the patient comments based on patient 
ID to make sure it ends up correctly on XNAT.

.. list-table:: Title
   :widths: 25 25 50
   :header-rows: 1

   * - Option
     - Values
     - Description
   * - server.storage
     - string
     -
   * - server.port
     - integer
     -
   * - pipeline.import_type
     - ['DICOM', 'Http']
     - Requires: import_port 
   * - pipeline.import_type
     - ['Directory', 'Archive']
     - Requires: import_path
   * - pipeline.import_port
     - integer
     - 
   * - pipeline.import_path
     - string
     -
    
   * - pipeline.export_type
     - ['Dicom', 'Http']
     - Requires: export_url
   * - pipeline.export_type
     - ['Directory', 'File']
     - Requires: export_path
   * - pipeline.export_url
     - string
     -
   * - pipeline.export_path
     - string
     - 
   * - project.project_id
     - string
     - XNAT project ID
   * - project.uid_root
     - dicom root UID
     - UID for hashing UID's in dicom

