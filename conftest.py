

def pytest_report_header(config):
    extra_header = []
    try:
        import ctp_builder

        extra_header.extend(
            [
                f"Testing ctp-builder version {ctp_builder.version.version} (built at {ctp_builder.version.build_date})",
                f"Based on {ctp_builder.version.git_revision} commit to branch {ctp_builder.version.git_branch} at {ctp_builder.version.git_revision_date}",
            ]
        )
    except ImportError as exception:
        extra_header.append(f"Cannot find ctp-builder version information: {exception}")

    return extra_header
